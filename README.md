# Overview #
**AlgoBench** is a software designed to demonstrate the runtime complexity of some common algorithms in the light of the "Algorithms and Data Structures" course (Inf2B) offered by the School of Informatics, University of Edinburgh. It involves configuring and executing algorithms with large inputs to generate runtimes that underscore the theoretical complexity of the algorithm.

It has two parts: this is **AlgoBench_B**, the C++ backend which actually runs the algorithms. The frontend, written in Java is called [**AlgoBench**](https://bitbucket.org/prof_zeezy/algobench), like the project. ;) That's where users can configure the algorithms to run, and the frontend handles the rest, including launching and communicating with the backend to get the task done. Also the results are viewed on the frontend.

**Version**: 1.0.0

# Running the software #
The [Downloads](https://bitbucket.org/prof_zeezy/algobench/downloads) section contains packaged distros for Windows, Linux, and Mac operating systems. To run the app, unzip the package and execute the "run" script from a terminal. That's all. (With time, it should be better packaged - .exe file for Windows and launchable jar for Linux, for example).

It requires the Java runtime (Java 8 at least).

# Building from the Source #
The project is developed using Netbeans IDE. 

**Requires**: Java 8 and a C++11-compliant compiler.

To checkout a copy, from the Netbeans IDE
Team menu select Git->Clone. Enter `https://prof_zeezy@bitbucket.org/prof_zeezy/algobench.git` as the Repository URL, and leave the User and Password fields blank. Finish up as normal.

The software requires two arguments to start: the path to the top-level directory (`algobench` if the defaults were used in setting up the repo); and the fully qualified filename of the executable of the C++ backend ([AlgoBench_B](https://bitbucket.org/prof_zeezy/algobench_b)) portion. These can be set up on the project settings (Right-click on the project in Netbeans IDE and select **Properties**->**Run**. In the **Arguments** section, type in the two paths separated by a space.)

The project scripts (`build.xml`, `nbproject/project.properties`) were modified to allow the packaging of the entire software during build. This basically involved:

* changing the distribution folder name to inf2b_algobench (the dist.dir variable in project.properties);
* adding `-post-jar` and `-post-clean` actions to the build file to copy the "run" script file (`run.bat` or `run.sh` - depending on the OS - found in the top-level project folder) into the distribution (`inf2b_algobench`) folder. Also, the executable for the backend is copied to this folder.
 
**Note**: You'll have to edit the build file to point to the folder where your backend executable is. Just edit the portion similar to the code shown below (replace `/path/to/backend/executable/` with the correct folder):

    <copy todir="${dist.dir}">
        <fileset dir="/path/to/backend/executable/" includes="algobench_b*"/>
    </copy>`

The inf2b_algobench folder can then be zipped and distributed after a build. It will contain everything needed to run the app by running `run.bat` (Windows) or `run.sh` (Linux/Mac) from a terminal.

**Note 2**: The C++ backend would require linking `ws2_32` library. This is already done in the if using Netbeans with one of the preconfigured IDE settings present in this sourcecode. If using Visual Studio, the linker settings should be modified too - just add `#pragma comment(lib,"Ws2_32.lib")` to the top of the main file (`inf2b.cpp`) or use the IDE linker settings.
 
# Contact #
Repo owner and admin: **Eziama Ubachukwu** (eziama[dot]ubachukwu[at]gmail[dot]com)