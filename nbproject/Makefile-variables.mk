#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
CND_BUILDDIR=build
CND_DISTDIR=dist
# Debug-L-ClientMode configuration
CND_PLATFORM_Debug-L-ClientMode=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug-L-ClientMode=dist/Debug-L-ClientMode/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug-L-ClientMode=algobench_
CND_ARTIFACT_PATH_Debug-L-ClientMode=dist/Debug-L-ClientMode/GNU-Linux-x86/algobench_
CND_PACKAGE_DIR_Debug-L-ClientMode=dist/Debug-L-ClientMode/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug-L-ClientMode=algobench.tar
CND_PACKAGE_PATH_Debug-L-ClientMode=dist/Debug-L-ClientMode/GNU-Linux-x86/package/algobench.tar
# Debug-L-ServerMode configuration
CND_PLATFORM_Debug-L-ServerMode=GNU-Linux-x86
CND_ARTIFACT_DIR_Debug-L-ServerMode=dist/Debug-L-ServerMode/GNU-Linux-x86
CND_ARTIFACT_NAME_Debug-L-ServerMode=algobench_
CND_ARTIFACT_PATH_Debug-L-ServerMode=dist/Debug-L-ServerMode/GNU-Linux-x86/algobench_
CND_PACKAGE_DIR_Debug-L-ServerMode=dist/Debug-L-ServerMode/GNU-Linux-x86/package
CND_PACKAGE_NAME_Debug-L-ServerMode=algobench.tar
CND_PACKAGE_PATH_Debug-L-ServerMode=dist/Debug-L-ServerMode/GNU-Linux-x86/package/algobench.tar
# Release configuration
CND_PLATFORM_Release=Cygwin-Windows
CND_ARTIFACT_DIR_Release=dist/Release/Cygwin-Windows
CND_ARTIFACT_NAME_Release=algobench_
CND_ARTIFACT_PATH_Release=dist/Release/Cygwin-Windows/algobench_
CND_PACKAGE_DIR_Release=dist/Release/Cygwin-Windows/package
CND_PACKAGE_NAME_Release=algobench.tar
CND_PACKAGE_PATH_Release=dist/Release/Cygwin-Windows/package/algobench.tar
# Release-Linux configuration
CND_PLATFORM_Release-Linux=GNU-Linux-x86
CND_ARTIFACT_DIR_Release-Linux=dist/Release-Linux/GNU-Linux-x86
CND_ARTIFACT_NAME_Release-Linux=algobench_
CND_ARTIFACT_PATH_Release-Linux=dist/Release-Linux/GNU-Linux-x86/algobench_
CND_PACKAGE_DIR_Release-Linux=dist/Release-Linux/GNU-Linux-x86/package
CND_PACKAGE_NAME_Release-Linux=algobench.tar
CND_PACKAGE_PATH_Release-Linux=dist/Release-Linux/GNU-Linux-x86/package/algobench.tar
# Debug-ClientMode configuration
CND_PLATFORM_Debug-ClientMode=Cygwin-Windows
CND_ARTIFACT_DIR_Debug-ClientMode=dist/Debug-ClientMode/Cygwin-Windows
CND_ARTIFACT_NAME_Debug-ClientMode=algobench_
CND_ARTIFACT_PATH_Debug-ClientMode=dist/Debug-ClientMode/Cygwin-Windows/algobench_
CND_PACKAGE_DIR_Debug-ClientMode=dist/Debug-ClientMode/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug-ClientMode=algobench.tar
CND_PACKAGE_PATH_Debug-ClientMode=dist/Debug-ClientMode/Cygwin-Windows/package/algobench.tar
# Debug-ServerMode configuration
CND_PLATFORM_Debug-ServerMode=Cygwin-Windows
CND_ARTIFACT_DIR_Debug-ServerMode=dist/Debug-ServerMode/Cygwin-Windows
CND_ARTIFACT_NAME_Debug-ServerMode=algobench_
CND_ARTIFACT_PATH_Debug-ServerMode=dist/Debug-ServerMode/Cygwin-Windows/algobench_
CND_PACKAGE_DIR_Debug-ServerMode=dist/Debug-ServerMode/Cygwin-Windows/package
CND_PACKAGE_NAME_Debug-ServerMode=algobench.tar
CND_PACKAGE_PATH_Debug-ServerMode=dist/Debug-ServerMode/Cygwin-Windows/package/algobench.tar
# Release-Mac configuration
CND_PLATFORM_Release-Mac=GNU-MacOSX
CND_ARTIFACT_DIR_Release-Mac=dist/Release-Mac/GNU-MacOSX
CND_ARTIFACT_NAME_Release-Mac=algobench_b
CND_ARTIFACT_PATH_Release-Mac=dist/Release-Mac/GNU-MacOSX/algobench_b
CND_PACKAGE_DIR_Release-Mac=dist/Release-Mac/GNU-MacOSX/package
CND_PACKAGE_NAME_Release-Mac=algobenchb.tar
CND_PACKAGE_PATH_Release-Mac=dist/Release-Mac/GNU-MacOSX/package/algobenchb.tar
#
# include compiler specific variables
#
# dmake command
ROOT:sh = test -f nbproject/private/Makefile-variables.mk || \
	(mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk)
#
# gmake command
.PHONY: $(shell test -f nbproject/private/Makefile-variables.mk || (mkdir -p nbproject/private && touch nbproject/private/Makefile-variables.mk))
#
include nbproject/private/Makefile-variables.mk
