#
# Generated Makefile - do not edit!
#
# Edit the Makefile in the project folder instead (../Makefile). Each target
# has a -pre and a -post target defined where you can add customized code.
#
# This makefile implements configuration specific macros and targets.


# Environment
MKDIR=mkdir
CP=cp
GREP=grep
NM=nm
CCADMIN=CCadmin
RANLIB=ranlib
CC=gcc
CCC=g++
CXX=g++
FC=gfortran
AS=as

# Macros
CND_PLATFORM=Cygwin-Windows
CND_DLIB_EXT=dll
CND_CONF=Debug-ServerMode
CND_DISTDIR=dist
CND_BUILDDIR=build

# Include project Makefile
include Makefile

# Object Directory
OBJECTDIR=${CND_BUILDDIR}/${CND_CONF}/${CND_PLATFORM}

# Object Files
OBJECTFILES= \
	${OBJECTDIR}/src/Algorithm.o \
	${OBJECTDIR}/src/Client.o \
	${OBJECTDIR}/src/ClientPipe.o \
	${OBJECTDIR}/src/Communicator.o \
	${OBJECTDIR}/src/Heapsort.o \
	${OBJECTDIR}/src/Quicksort.o \
	${OBJECTDIR}/src/Server.o \
	${OBJECTDIR}/src/ServerPipe.o \
	${OBJECTDIR}/src/global.o \
	${OBJECTDIR}/src/inf2b.o \
	${OBJECTDIR}/src/stdafx.o


# C Compiler Flags
CFLAGS=

# CC Compiler Flags
CCFLAGS=
CXXFLAGS=

# Fortran Compiler Flags
FFLAGS=

# Assembler Flags
ASFLAGS=

# Link Libraries and Options
LDLIBSOPTIONS=

# Build Targets
.build-conf: ${BUILD_SUBPROJECTS}
	"${MAKE}"  -f nbproject/Makefile-${CND_CONF}.mk ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algobench_.exe

${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algobench_.exe: ${OBJECTFILES}
	${MKDIR} -p ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}
	${LINK.cc} -o ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algobench_ ${OBJECTFILES} ${LDLIBSOPTIONS} -lws2_32

${OBJECTDIR}/src/Algorithm.o: nbproject/Makefile-${CND_CONF}.mk src/Algorithm.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Algorithm.o src/Algorithm.cpp

${OBJECTDIR}/src/Client.o: nbproject/Makefile-${CND_CONF}.mk src/Client.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Client.o src/Client.cpp

${OBJECTDIR}/src/ClientPipe.o: nbproject/Makefile-${CND_CONF}.mk src/ClientPipe.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ClientPipe.o src/ClientPipe.cpp

${OBJECTDIR}/src/Communicator.o: nbproject/Makefile-${CND_CONF}.mk src/Communicator.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Communicator.o src/Communicator.cpp

${OBJECTDIR}/src/Heapsort.o: nbproject/Makefile-${CND_CONF}.mk src/Heapsort.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Heapsort.o src/Heapsort.cpp

${OBJECTDIR}/src/Quicksort.o: nbproject/Makefile-${CND_CONF}.mk src/Quicksort.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Quicksort.o src/Quicksort.cpp

${OBJECTDIR}/src/Server.o: nbproject/Makefile-${CND_CONF}.mk src/Server.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/Server.o src/Server.cpp

${OBJECTDIR}/src/ServerPipe.o: nbproject/Makefile-${CND_CONF}.mk src/ServerPipe.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/ServerPipe.o src/ServerPipe.cpp

${OBJECTDIR}/src/global.o: nbproject/Makefile-${CND_CONF}.mk src/global.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/global.o src/global.cpp

${OBJECTDIR}/src/inf2b.o: nbproject/Makefile-${CND_CONF}.mk src/inf2b.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/inf2b.o src/inf2b.cpp

${OBJECTDIR}/src/stdafx.o: nbproject/Makefile-${CND_CONF}.mk src/stdafx.cpp 
	${MKDIR} -p ${OBJECTDIR}/src
	${RM} "$@.d"
	$(COMPILE.cc) -g -std=c++11 -MMD -MP -MF "$@.d" -o ${OBJECTDIR}/src/stdafx.o src/stdafx.cpp

# Subprojects
.build-subprojects:

# Clean Targets
.clean-conf: ${CLEAN_SUBPROJECTS}
	${RM} -r ${CND_BUILDDIR}/${CND_CONF}
	${RM} ${CND_DISTDIR}/${CND_CONF}/${CND_PLATFORM}/algobench_.exe

# Subprojects
.clean-subprojects:

# Enable dependency checking
.dep.inc: .depcheck-impl

include .dep.inc
