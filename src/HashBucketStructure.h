/*
  * The MIT License
  *
  * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
#pragma once
#include "stdafx.h"
#include "global.h"

namespace inf2b
{

    class HashBucketStructure {
    public:
        HashBucketStructure(size_t& s, size_t&& numElems) : arraySize {s}, numElements {numElems} {
            // MS VS 2013 C++11 compiler bug (error C2979) doesn't allow list initialization
            // inside member initializer list, hence the workaround here.
            hashBucket.resize(s);
        };

        void addEntry(size_t hashValue, std::pair<std::string, std::string>&& entry) {
            auto i = hashValue % arraySize;
            hashBucket[i].insert(std::move(entry));
        };

        void addEntry(size_t hashValue, std::pair<std::string, std::string>& entry) {
            auto i = hashValue % arraySize;
            hashBucket[i].insert(std::move(entry));
        };

        size_t size() {
            return arraySize;
        }

        void printBucket(std::ostream& outputStream) {
            size_t i = 0,
                minSize = numElements + 1,
                maxSize = 0;
            outputStream << "BUCKET\tSIZE\tELEMENTS (K:V)\n";
            for (auto& dict : hashBucket) {
                auto s = dict.size();
                if (s > maxSize) {
                    maxSize = s;
                }
                if (s < minSize) {
                    minSize = s;
                }
                outputStream << i << "\t" << s << "\t";
                for (auto& e : dict) {
                    outputStream << e.first << ":" << e.second << ",";
                }
                // the space is a placeholder so that splitting on \t still works well
                // for empty buckets
                outputStream << " \n";
                ++i;
            }
            std::cout << "[MINBUCKETSIZE]\t" << minSize;
            std::cout << "\n[MAXBUCKETSIZE]\t" << maxSize << std::endl;
            outputStream.flush();
        }

        HashBucketStructure(const HashBucketStructure& rhs) = delete;
        HashBucketStructure(HashBucketStructure&& rhs) = delete;
        HashBucketStructure& operator=(const HashBucketStructure& rhs) = delete;
        HashBucketStructure& operator=(HashBucketStructure&& rhs) = delete;
        ~HashBucketStructure() { };

    private:
        size_t arraySize;
        size_t numElements;
        std::vector<HashDictionaryType> hashBucket;
    };

}