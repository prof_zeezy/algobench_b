/*
  * The MIT License
  *
  * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
#pragma once
#include "stdafx.h"
#include "global.h"
#include "HashBucketStructure.h"

namespace inf2b
{
    class Hashing {
    private:
        long recursionCounter;
        long executionCount;
        long maxRecursionDepth;
        int hashType;
        std::ofstream outputFile;
        HashBucketStructure bucket;
        //HashFunctionType& hashFunction;
        PairList& input;
        std::string& output;

        void printResults() {
            std::stringstream ss;
            auto t = time(NULL);
            outputFile.open("output/hashing" + std::to_string(t) + ".txt");
            bucket.printBucket(outputFile);
            //bucket.printBucket(ss);
            //output = ss.str();
        };

    public:
        // TODO: allow custom functions to be supplied from frontend
        Hashing(PairList& input, size_t bucketSize, int hashType, std::string& output)
            : input(input), bucket {bucketSize, input.size()}, hashType {hashType}, output(output) { }

        Hashing(Hashing& rhs) = delete;
        ~Hashing() { outputFile.close(); }

        size_t defaultGoodHash(const std::string& key) {
            size_t hashValue = 0;
            for (char c : key) {
                hashValue = 31 * hashValue + c;
            }
            return hashValue;
        }

        size_t defaultBadHash(const std::string& key) {
            std::hash<unsigned char> hashFunc;
            return hashFunc(key[0]);
        }

        void operator()() {
            std::cout << "[TASKRUNNER] Hashing..." << std::endl;
            //if (hashFunction == NULL) {
            // throws an error. deal with it later
            // hashFunction = isGoodHash ? &Hashing::defaultGoodHash : &Hashing::defaultBadHash;
            //}
            bool good = hashType == GOOD_HASH;
            // keep count
            size_t  count { };
            for (auto e : input) {
                auto hash = good ? defaultGoodHash(e.first) : defaultBadHash(e.first);
                bucket.addEntry(hash, e);
            }
            std::cout << "[TASKRUNNER] Printing results..." << std::endl;
            printResults();
        }
    };
}