/*
  * The MIT License
  *
  * Copyright 2015 Eziama Ubachukwu (eziama.ubachukwu@gmail.com).
  *
  * Permission is hereby granted, free of charge, to any person obtaining a copy
  * of this software and associated documentation files (the "Software"), to deal
  * in the Software without restriction, including without limitation the rights
  * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
  * copies of the Software, and to permit persons to whom the Software is
  * furnished to do so, subject to the following conditions:
  *
  * The above copyright notice and this permission notice shall be included in
  * all copies or substantial portions of the Software.
  *
  * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
  * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
  * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
  * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
  * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
  * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
  * THE SOFTWARE.
  */
#pragma once
#include "stdafx.h"
#include "global.h"
#include "GraphStructure.h"

namespace inf2b
{
    class GraphAlgo {
    public:
        class BFS {
        public:
            BFS(Graph& g) : graph(g), counter { }, longestPath { } { };
            void operator()() {

                std::cout << "Executing BFS..." << std::endl;
                auto size = graph.getNumVertices();
                int treeCount = 0;
                for (auto& v : graph.getVertices()) {
                    if (v.getState() == Colour.white) {
                        bfsFromVertex(v);
                        ++treeCount;
                    }
                }
                std::cout << "Number of trees in graph forest: " << treeCount <<
                    "\n=============================" << std::endl;
            };
        private:
            Graph& graph;
            size_t counter;
            // TODO: logic not yet fixed. Leaving it for now...
            size_t longestPath;

            void bfsFromVertex(Vertex& v) {
                v.setState(Colour.black);
                // reset for each tree: first node touched here
                counter = 1;
                std::queue<Vertex*> vQueue;
                vQueue.push(&v);
                while (!vQueue.empty()) {
                    // get a pointer to the first element
                    auto& pV = vQueue.front();
                    vQueue.pop();
                    for (auto& v : pV->getNeighbours()) {
                        if (v->getState() == Colour.white) {
                            v->setState(Colour.black);
                            vQueue.push(std::move(v));
                            //// count how many nodes touched
                            //++counter;
                            //if (counter > longestPath) {
                            //    longestPath;
                            //}
                        }
                    }
                }
            }
        };

        class DFS {
        public:
            DFS(Graph& g) : graph(g), counter { } { };
            void operator()() {
                std::cout << "Executing DFS..." << std::endl;
                auto size = graph.getNumVertices();
                int treeCount = 0;
                for (auto& v : graph.getVertices()) {
                    if (v.getState() == Colour.white) {
                        ++treeCount;
                        dfsFromVertex(v);
                    }
                }
                std::cout << "Number of trees in graph forest: " << treeCount <<
                    "\n=============================" << std::endl;
            };
        private:
            Graph& graph;
            size_t counter;

            void dfsFromVertex(Vertex& v) {
                std::stack<Vertex*> vStack;
                vStack.push(&v);
                while (!vStack.empty()) {
                    // get a pointer to the first element
                    auto pV = vStack.top();
                    vStack.pop();
                    if (pV->getState() == Colour.white) {
                        // count how many nodes touched
                        ++counter;
                        pV->setState(Colour.black);

                        for (auto& v : pV->getNeighbours()) {
                            vStack.push(std::move(v));
                        }
                    }
                }
            }
        };
    };
}