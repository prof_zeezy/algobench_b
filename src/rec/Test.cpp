/* 
 * File:   Test.cpp
 * Author: eziama
 * 
 * Created on June 6, 2015, 8:04 AM
 */
#include <iostream>
#include "Test.h"

template<class T>
Test<T>::Test() {}

template<class T>
Test<T>::Test(size_t size) : size_(size) {};

template<class T>
Test<T>::Test(const Test& orig) {};

template<class T>
Test<T>::~Test() {};

template<class T>
void Test<T>::sayHello(T* in) {
    int i;
    // leaving this bit for now
	//    input = (T *) malloc(sizeof (T) * size_t); // something's wrong here.
	//    // or use the new operator
	//    for (i = 0; i < size_t; i++) {
	//        input[i] = in[i];
	//    }
    std::cout << "Hello";
}
