/* 
 * File:   main.cpp
 * Author: eziama
 *
 * Created on June 1, 2015, 1:03 AM
 */

#include <cstdlib>
#include <iostream>
#include <math.h>
#include "Test.h"

using namespace std;

int* generate_random_ints(int count, int min, int max) {
    int numbers[10];

    for (int i = 0; i < count; ++i) {
        numbers[i] = min + (double) rand() / RAND_MAX * (max - min);
        cout << i << ":" << numbers[i] << ", ";
    }

    return numbers;
}

/*
 * The entry point
 */
int main(int argc, char** argv) {
    int* inputArray = generate_random_ints(10, 50, 100);
    
    // Test<int> t (inputArray); <--- this is the problem, but to fix it, one need to know what it needs to do
    system("PAUSE");
    return 0;
}

